from PIL import Image
import numpy as np
import io


def encrypt_ecb():
    with open("key.txt", "rb") as key_file:
        key = key_file.read()

    plain = Image.open("plain.bmp", "r")

    # print(plain.size, plain.format, plain.mode)

    plain_pixels = np.asarray(plain).tobytes()
    width, height = plain.size
    channels = 3 if plain.mode == "RGB" else 4

    print(width, height)
    # block_width = 4
    # block_height = 4
    key_len = len(key)

    block_len = key_len
    # print(len(plain_pixels), key_len)

    block = io.BytesIO()
    encrypted = bytearray([])

    for i in range(block_len, len(plain_pixels) + 1, block_len):
        block = plain_pixels[i - block_len:i]

        for n in range(block_len):
            encrypted.append(block[n] ^ key[n])

    if len(encrypted) != len(plain_pixels):
        for i in range(len(encrypted), len(plain_pixels), 1):
            encrypted.append(ord("~"))
    if plain.mode == "L":
        encrypted = np.array(encrypted).reshape([height, width]).astype(np.uint8)
    else:
        encrypted = np.array(encrypted).reshape([height, width, channels]).astype(np.uint8)
    print(encrypted.shape)
    crypto = Image.fromarray(encrypted).convert("L")
    crypto.save("ecb_crypto.bmp")
    plain.close()
    crypto.show()
    crypto.close()


def encrypt_cbc():
    with open("key.txt", "rb") as key_file:
        key = key_file.read()

    plain = Image.open("plain.bmp", "r")
    # print(plain.size, plain.format, plain.mode)
    plain_pixels = np.asarray(plain).tobytes()
    width, height = plain.size
    channels = 3 if plain.mode == "RGB" else 4
    print(width, height)

    # block_width = 4
    # block_height = 4
    key_len = len(key)

    block_len = key_len
    # print(len(plain_pixels), key_len)
    encrypted = bytearray([])

    for i in range(key_len, len(plain_pixels) + 1, key_len):
        block = io.BytesIO()
        block = plain_pixels[i - key_len:i]
        for n in range(key_len):
            encrypted.append(block[n] ^ key[n])
        key = encrypted[i - key_len:i]
    if len(encrypted) != len(plain_pixels):
        for i in range(len(encrypted), len(plain_pixels), 1):
            encrypted.append(ord("~"))
    if plain.mode == "L":
        encrypted = np.array(encrypted).reshape([height, width]).astype(np.uint8)
    else:
        encrypted = np.array(encrypted).reshape([height, width, channels]).astype(np.uint8)

    # print(encrypted.shape)
    crypto = Image.fromarray(encrypted).convert("L")
    crypto.save("cbc_crypto.bmp")
    plain.close()
    crypto.show()
    crypto.close()


encrypt_ecb()
encrypt_cbc()
