md5sum personal.txt | awk '{print "md5sum: " $1}' | cat > hash.txt
sha1sum personal.txt | awk '{print "sha1sum: " $1}' | cat >> hash.txt
sha224sum personal.txt | awk '{print "sha224sum: " $1}' | cat >> hash.txt
sha256sum personal.txt | awk '{print "sha256sum: " $1}' | cat >> hash.txt
sha384sum personal.txt | awk '{print "sha384sum: " $1}' | cat >> hash.txt
sha512sum personal.txt | awk '{print "sha512sum: " $1}' | cat >> hash.txt
b2sum personal.txt | awk '{print "b2sum: " $1}' | cat >> hash.txt