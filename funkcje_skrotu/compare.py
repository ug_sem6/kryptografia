from sys import argv
import os

with open("hash.txt", "r") as hash_file:
    text = [line[:-4] for line in hash_file.readlines()]

line1 = text[-1]
line2 = text[-2]
diff_counter = 0

bit_len = 0
for idx in range(len(line1)):
    c1 = bin(int(line1[idx], 16))[2:].zfill(4)
    c2 = bin(int(line2[idx], 16))[2:].zfill(4)

    for bit1, bit2 in zip(c1, c2):
        if bit1 != bit2:
            diff_counter += 1
        bit_len += 1

diff_percentage = diff_counter / bit_len * 100


with open("diff.txt", "r+") as diff:
    if len(list(diff.readlines())) > 40:
        diff.seek(0)
        diff.truncate()
    diff.write('\n'.join([argv[1], argv[2], line1, line2]))
    diff.write(
        f"\nIlość różniących sie bitów: {diff_counter} z {bit_len}, Procentowo: {round(diff_percentage, 2)}% \n\n")
