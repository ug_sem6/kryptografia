from sys import argv
import re

line_len = 56

def prepare():
    orig_text = ""
    with open("orig.txt", "r") as orig_file:
        orig_text = orig_file.read().lower()
        orig_text = re.sub("[^A-Za-z0-9 ]+", "", orig_text)
        orig_text = [orig_text[i: i + line_len] for i in range(0, len(orig_text), line_len)]
        orig_text = list(filter(lambda l: l != "", orig_text))
        if len(orig_text[-1]) < line_len:
            del orig_text[-1]
    with open("plain.txt", "w") as plain_file:
        plain_file.write("\n".join(orig_text))


def encrypt():
    with open("plain.txt", "r") as plain_file:
        plain_text = plain_file.read()
    plain_text = plain_text.split(sep="\n")
    with open("key.txt", "r") as key_file:
        key = [ord(c) for c in key_file.read()]
    with open("crypto.txt", "w") as crypto_file:
        encrypted_text = []
        for line in plain_text:
            plain_text_int = [ord(c) for c in line]
            encrypted_text.append(
                "".join([chr(key[i] ^ plain_text_int[i]) for i in range(len(key))]))
        crypto_file.write("\n".join(encrypted_text))


def kryptoanaliza():
    k = [0] * line_len
    with open('crypto.txt', 'rb') as crypto_file:
        crypto_text = crypto_file.read().decode('utf-8').split("\n")
    for i in range(len(crypto_text)):
        crypto_line = crypto_text[i]
        for j in range(line_len):
            if ord(crypto_line[j]) >= 96:
                k[j] = ord(crypto_line[j]) ^ 32
    kp = ''.join([chr(c) if c else '?' for c in k])
    print("Znaleziony klucz: ", kp)
    with open("decrypt.txt", "w") as decrypt_file:
        decrypted_text = []
        for line in crypto_text:
            plain_text_int = [ord(c) for c in line]
            decrypted_text.append(
                "".join([chr(ord(kp[i]) ^ plain_text_int[i]) for i in range(len(kp))]))
        decrypt_file.write("\n".join(decrypted_text))


if __name__ == "__main__":

    opts = {
        "-p": prepare,
        "-e": encrypt,
        "-k": kryptoanaliza
    }

    if len(argv) < 2:
        print("Za mało argumentów")
        exit(0)
    if argv[1] not in opts.keys():
        exit("Niepoprawny argument")

    opts[argv[1]]()
