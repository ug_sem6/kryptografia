###
# Zadanie z kryptografii - szyfr Cezar i afiniczny
# Tomasz Wolak, 253996

###

from sys import exit, argv
import string
import os

alf_lower = string.ascii_lowercase
alf_upper = string.ascii_uppercase

alf_size = len(alf_lower)


def cezar(opt):
	# pobranie klucza (z walidacją)
	def get_key():
		try:
			with open("key.txt", "r") as key_file:
				key = key_file.read(1)
				key = int(key) if key.isdigit() else -1
			if key == -1:
				raise ValueError
			return key
		except ValueError:
			exit("Niepoprawny klucz")

	def encrypt_text():
		key = get_key()
		with open("plain.txt", "r") as plain:
			plain_text = plain.read()
		with open("crypto.txt", "w") as crypto:
			for c in plain_text:
				if c in string.whitespace or c is None:
					crypto.write(c)  # biały znak bez zmian
				else:
					alf = alf_lower if c in alf_lower else alf_upper
					crypto.write(alf[(alf.index(c) + key) % alf_size])
		with open("extra.txt", "w") as extra:
			extra.write(plain_text[:2])

	def decrypt_text():
		key = get_key()
		with open("crypto.txt", "r") as crypto:
			with open("decrypt.txt", "w") as decrypt:
				for c in crypto.read():
					if c in string.whitespace:
						decrypt.write(c)  # biały znak bez zmian
					else:
						alf = alf_lower if c in alf_lower else alf_upper
						decrypt.write(alf[(alf.index(c) - key) % alf_size])

	def kryptoanaliza_jawne():
		with open("crypto.txt", "r") as crypto:
			crypto_text = crypto.read()
		with open("extra.txt", "r") as extra:
			extra_text = extra.read()

		# odnalezienie klucza
		keys = []
		for i in range(len(extra_text)):
			alf = alf_lower if extra_text[i] in alf_lower else alf_upper
			keys.append((alf.index(crypto_text[i]) - alf.index(extra_text[i])) % alf_size)

		# odszyfrowanie tekstu ze znalezionym kluczem
		if keys[0] == keys[1]:
			decrypt = ""
			for c in crypto_text:
				alf = alf_lower if c in alf_lower else alf_upper
				decrypt += alf[(alf.index(c) - keys[0]) % alf_size] if c not in string.whitespace else c

			# zapisanie zaszyfrowanego tekstu i klucza
			with open("decrypt.txt", "w") as decrypt_file:
				decrypt_file.write(decrypt)
			with open("key-found.txt", "w") as key_file:
				key_file.write(str(keys[0]))
		else:
			exit("Nie znaleziono klucza.\n")

	def kryptoanaliza_kryptogram():
		with open("crypto.txt", "r") as crypto:
			crypto_text = crypto.read()
		with open("decrypt.txt", "w") as decrypt_file:
			for k in range(alf_size):
				decrypt = ""
				for c in crypto_text:
					alf = alf_lower if c in alf_lower else alf_upper
					decrypt += alf[(alf.index(c) - k) % alf_size] if c not in string.whitespace else c
				# zapisanie zaszyfrowanego tekstu
				decrypt_file.write(f"{k}: {decrypt}\n")

	opts = {
		"-e": encrypt_text,
		"-d": decrypt_text,
		"-j": kryptoanaliza_jawne,
		"-k": kryptoanaliza_kryptogram
	}
	if opt not in opts.keys():
		exit("Podano złe parametry.")
	opts.get(opt)()


def afiniczny(opt):
	def nwd(x, y=alf_size):
		while y != 0:
			z = x % y
			x = y
			y = z
		return x

	# funkcja walidująca klucz
	def is_valid(key):
		return nwd(key[0]) == 1 and (key[0] * reverse(key[0])) % alf_size == 1

	def get_key():
		try:
			with open("key.txt", "r") as key:
				key = [int(k) if k.isdigit() else -1 for k in key.read().split()]
			if -1 in key or not is_valid(key):
				raise ValueError
			if is_valid(key):
				return key
		except ValueError:
			exit("Niepoprawny klucz")

	# odwrotność w pierścieniu Z26
	def reverse(a):
		result = None
		for i in range(1, alf_size):
			if (a * i) % alf_size == 1 and nwd(i) == 1:
				result = i
		return result

	def encrypt_text():
		key = get_key()
		with open("plain.txt", "r") as plain:
			plain_text = plain.read()
		with open("crypto.txt", "w") as crypto:
			for c in plain_text:
				if c in string.whitespace or c is None:
					crypto.write(c)  # biały znak bez zmian
				else:
					alf = alf_lower if c in alf_lower else alf_upper
					crypto.write(alf[(key[0] * alf.index(c) + key[1]) % alf_size])
		with open("extra.txt", "w") as extra:
			extra.write(plain_text[:2])

	def decrypt_text():
		key = get_key()
		with open("crypto.txt", "r") as crypto:
			with open("decrypt.txt", "w") as decrypt:
				for c in crypto.read():
					if c in string.whitespace:
						decrypt.write(c)
					else:
						alf = alf_lower if c in alf_lower else alf_upper
						decrypt.write(alf[(reverse(key[0]) * (alf.index(c) - key[1])) % alf_size])

	def kryptoanaliza_jawne():
		with open("crypto.txt", "r") as crypto:
			crypto_text = crypto.read()
		with open("extra.txt", "r") as extra:
			extra_text = extra.read()

		alf = [alf_lower if c in alf_lower else alf_upper for c in crypto_text[:2]]
		keys = []
		for a in range(1, alf_size, 2):
			if a == 13:
				continue
			keys = []
			for i in range(len(extra_text)):
				b = int(alf[i].index(crypto_text[i]) - a * alf[i].index(extra_text[i])) % alf_size
				keys.append((a, b))
			if keys[0][0] == keys[1][0] and keys[0][1] == keys[1][1]:
				break
		key = keys[0]
		if is_valid(key):
			with open("key-found.txt", "w") as key_found:
				key_found.write(f"{key[0]} {key[1]}")
				with open("decrypt.txt", "w") as decrypt:
					for c in crypto_text:
						if c in string.whitespace:
							decrypt.write(c)
						else:
							alf = alf_lower if c in alf_lower else alf_upper
							decrypt.write((alf[int(reverse(key[0]) * (alf.index(c) - key[1])) % alf_size]))

	def kryptoanaliza_kryptogram():
		with open("crypto.txt", "r") as crypto:
			crypto_text = crypto.read()
		with open("decrypt.txt", "w") as decrypt_file:
			for a in range(1, alf_size, 2):
				if a == 13:
					continue
				for b in range(alf_size):
					decrypt = ""
					for c in crypto_text:
						if c in string.whitespace:
							decrypt += c
							continue
						alf = alf_lower if c in alf_lower else alf_upper
						decrypt += alf[(reverse(a) * (alf.index(c) - b)) % alf_size]
					# zapisanie zaszyfrowanego tekstu
					decrypt_file.write(f"({a}, {b}): {decrypt}\n")

	opts = {
		"-e": encrypt_text,
		"-d": decrypt_text,
		"-j": kryptoanaliza_jawne,
		"-k": kryptoanaliza_kryptogram
	}
	if opt not in opts.keys():
		exit("Podano złe parametry.")
	opts.get(opt)()


if __name__ == "__main__":
	if len(argv) < 3:
		print("Za mało argumentów")
		exit(0)
	args = argv[1:]
	if args[0] == '-c':
		cezar(args[1])
	elif args[0] == '-a':
		afiniczny(args[1])
	else:
		exit("Podano złe parametry.")
	exit(0)
